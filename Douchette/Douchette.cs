﻿using System;
using System.Data.SQLite;
using System.IO;
namespace Douchette
{
    class Douchette
    {
        //Resume : Checks a bar code passed in parameter and store in a database (Write the code barre for the simulation)
        //In(): 
        //Out(): 
        static void Main(string[] args)
        {
            Console.WriteLine("Waiting Barcode...");
            string barcode = Console.ReadLine();
            if(barcode.Length==13)
            {
                string productType = "";
                int productYear = 0;
                string productRef = "";
                bool verificationConfirm;
                SeparateBarcode(ref productType, ref productYear, ref productRef, barcode);
                verificationConfirm = Verification(productType, productYear, productRef);
                if (verificationConfirm == true)
                {
                    bbdInsertion(productType, productYear, productRef);
                }
                else
                {
                    Console.Write("Bip Bip");
                    Console.ReadKey();
                }
            }
            
            else
            {
                Console.Write("Bip Bip");
            }
            Console.ReadKey();

        }
        //Resume : Separate the bar code into 3 different elements (Type,Year,Reference)
        //In(): the 3 variables that will contain the data and the barcode
        //Out(): 
        static void SeparateBarcode(ref string productType,ref int productYear,ref string productRef,string barcode)
        {
            productType = barcode.Substring(0, 3);
            productYear = int.Parse(barcode.Substring(4, 4));
            productRef = barcode.Substring(9, 4);
        }
        //Resume : Check that the 3 elements of the barcode are correct
        //In(): the type(string),Year(int) and Ref(string) of the barcode
        //Out(): boolean (true for correct and false for incorrect)
        static bool Verification(string productType, int productYear, string productRef)
        {
            bool boolYear, boolType, boolRef;
            boolType = TypeVerification(productType);
            boolYear = YearVerification(productYear);
            boolRef = RefVerification(productRef);
            if (boolType && boolYear && boolRef == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //Resume : Checks that the Barcode type contains only 3 letters
        //In(): Type of barcode (string)
        //Out(): boolean (true for correct and false for incorrect)
        static bool TypeVerification(string type)
        {
            int count = 0;
            for (int i = 0; i < 3; i++)
            {
                if (type[i] > 65 && type[i] < 90)
                {
                    count++;
                }
            }
            if (count == 3)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //Resume : Check that the date of barcode is less than or equal to the current one
        //In(): Year of barcode (int)
        //Out(): boolean (true for correct and false for incorrect)
        static bool YearVerification(int year)
        {

            if (year <= DateTime.Now.Year)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        //Resume : Checks that the Barcode type contains only 4 char (Number or letter)
        //In(): Reference of barcode (string)
        //Out(): boolean (true for correct and false for incorrect)
        static bool RefVerification(string productRef)
        {
            int count=0;
            for (int i = 0; i < productRef.Length; i++)
            {
                if ((productRef[i] >= 48 && productRef[i] <= 57 )|| (productRef[i] >= 65 && productRef[i] <= 90))
                {
                    count++;
                }
            }
            if (count == 4)
            {
                 return true;
            }
            else
            {
                return false;

            }
        }
        //Resume : Connect to a database and insert the data passed in parameters
        //In(): The type(string),Year(int) and Ref(string) of the barcode
        //Out():
        static void bbdInsertion(string productType,int productYear,string productRef)
        {
            try
            {
                string path = Path.GetFullPath("../../market.sqlite");
                SQLiteConnection database = new SQLiteConnection("Data Source=" + path);
                InsertCommand(database, productType, productYear, productRef);
            }
            catch
            {
                throw new Exception("Error in dataBase Insertion");
            }
        }
        //Resume : insert in a database the data passed in parameters
        //In(): The DataBase(SQLiteConnection) Type(string),Year(int) and Ref(string) of the barcode
        //Out():
        static void InsertCommand(SQLiteConnection database, string productType, int productYear, string productRef)
        {
            database.Open();
            SQLiteCommand command = new SQLiteCommand(database);
            command.CommandText = "INSERT INTO Inventory(Type,Year,Ref) VALUES (@Type,@Year,@Ref)";
            command.Prepare();
            command.Parameters.AddWithValue("@Type", productType);
            command.Parameters.AddWithValue("@Year", productYear);
            command.Parameters.AddWithValue("@Ref", productRef);
            command.ExecuteNonQuery();
            database.Close();
            Console.WriteLine("Bip");
        }
       
    }
}
